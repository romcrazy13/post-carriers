<?php

namespace App\Console\Commands;

use App\Models\Shipment;
use Illuminate\Console\Command;

class shipments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shipment:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get List Of Shipments';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        echo PHP_EOL . PHP_EOL;
        echo '|------|--------------------|-----------|-----------|' . PHP_EOL;
        echo '|  ID  |      CARRIER       |   WEIGHT  |   PRICE   |' . PHP_EOL;
        echo '|------|--------------------|-----------|-----------|' . PHP_EOL;

        foreach (Shipment::all() as $shipment){
            $data = $shipment->toArray();
            $id = sprintf("% 5s", $data['id']);
            $carrier = sprintf("%-19s", $data['carrier']['description']);
            $weight = sprintf("% 10s", $data['weight']);
            $price = sprintf("% 10s", $data['price']);
            echo "|$id | $carrier|$weight |$price |" . PHP_EOL;
        }

        echo '|------|--------------------|-----------|-----------|' . PHP_EOL;

        return 0;
    }
}
