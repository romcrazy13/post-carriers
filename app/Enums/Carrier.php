<?php

namespace App\Enums;

use App\Services\Carriers\DHL;
use App\Services\Carriers\PR;
use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Attributes\Description;

/**
 *
 * @property int $id
 * @property string $name
 * @property string $local_description
 *
 * @method static static PR()
 * @method static static DHL()
 */
final class Carrier extends Enum implements LocalizedEnum
{
    #[Description('Post Of Russia')]
    const PR    = 0;
    #[Description('DHL')]
    const DHL   = 1;

    private static array $serviceClass = [
        self::PR    => PR::class,
        self::DHL   => DHL::class,
    ];

    public static function parseDatabase($value): int
    {
        return (int) $value;
    }

    public function getService()
    {
        return new self::$serviceClass[$this->value]();
    }

    public function toArray(): mixed
    {
        return [
            'id'            => $this->value,
            'name'          => $this->key,
            'description'   => $this->description,
        ];
    }
}
