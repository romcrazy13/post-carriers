<?php

namespace App\Models;

use App\Enums\Carrier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $carrier_id
 * @property Carrier $carrier
 * @property int $weight
 * @property int $price
 */
class Shipment extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $guarded = [];

    protected $casts = [
        'carrier_id' => Carrier::class,
    ];

    public function carrier(): Carrier
    {
        return Carrier::fromValue($this->carrier_id);
    }

    public function toArray()
    {
        return [
            'id'            => $this->id,
            'carrier'       => $this->carrier()->toArray(),
            'weight'        => number_format($this->weight, 3, '.'),
            'price'         => number_format($this->price, 2, '.'),
        ];
    }
}
