<?php

namespace App\Services\Carriers;

interface CarrierInterface
{
    /**
     * @param float $weight
     * @return float
     */
    public static function getPrice(float $weight): float;

}
