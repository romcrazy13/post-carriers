<?php

namespace App\Services\Carriers;

class DHL implements CarrierInterface
{

    const COST = 100;

    /**
     * @inheritDoc
     */
    public static function getPrice(float $weight): float
    {
        return $weight * self::COST;
    }
}
