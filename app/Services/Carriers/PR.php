<?php

namespace App\Services\Carriers;

class PR implements CarrierInterface
{

    const WEIGHT_LIMIT = 10;
    const COST_DOWN_LIMIT = 100;
    const COST_UP_LIMIT = 1000;

    /**
     * @inheritDoc
     */
    public static function getPrice(float $weight): float
    {
        return $weight <= self::WEIGHT_LIMIT
            ? $weight * self::COST_DOWN_LIMIT
            : self::WEIGHT_LIMIT * self::COST_DOWN_LIMIT + ($weight - self::WEIGHT_LIMIT) * self::COST_UP_LIMIT;
    }
}
