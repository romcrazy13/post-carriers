<?php

namespace Database\Factories;

use App\Enums\Carrier;
use Faker\Provider\File;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Shipment>
 */
class ShipmentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     * @throws \Exception
     */
    public function definition()
    {
        $carrier = Carrier::getRandomInstance();
        $weight = random_int(1, 100);

        return [
            'carrier_id'    => $carrier->value,
            'weight'        => $weight,
            'price'         => $carrier->getService()->getPrice($weight),
        ];
    }
}
