<?php

use App\Enums\Carrier;

return [
    Carrier::class => [
        Carrier::PR => 'Post Of Russia',
        Carrier::DHL => 'DHL',
    ]
];

