<?php

use App\Enums\Carrier;

return [
    Carrier::class => [
        Carrier::PR => 'Почта России',
        Carrier::DHL => 'DHL',
    ]
];

